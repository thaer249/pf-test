# Property Finder Test

**Author**: Thaer Abbas

## How to run

`npm install` or `yarn`

**development**

`npm run dev`

**production**

`npm run build`

`npm start`

## Notes

Most libraries used are dev tooling, except for `lodash.throttle` and basic Js polyfills