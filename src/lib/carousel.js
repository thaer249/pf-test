import throttle from 'lodash.throttle';
import './carousel.sass';

const KEYS = {
  RIGHT: 'ArrowRight',
  LEFT: 'ArrowLeft'
};

const defaultOptions = {
  responsive: [
    {
      breakpoint: 1140,
      slides: 4
    },
    {
      breakpoint: 940,
      slides: 3
    },
    {
      breakpoint: 720,
      slides: 2
    }
  ]
};

class Carousel {
  constructor(element, options = {}) {

    this.options = Object.assign({}, defaultOptions, options);

    this.$element = document.querySelector(element);

    // create track element
    this.$track = document.createElement('div');
    this.$track.className = 'js-carousel-track';
    this.$element.appendChild(this.$track);

    this.$container = this.$element.querySelector('ul');
    // put the list inside the track
    this.$track.appendChild(this.$container);
    this.$panes = this.$container.querySelectorAll('li');

    this.currentSlide = 0;
    this.slidesToShow = 4;
    this.numberOfSlides = this.$panes.length / this.slidesToShow;

    this.init();
  }

  init() {
    this.$track.style.overflow = 'hidden';

    requestAnimationFrame(this.setDimensions.bind(this));

    // weird scenario where viewportWidth has different values on load and on resize
    // @todo investigate
    if (this.viewportWidth < 720) {
      requestAnimationFrame(this.setDimensions.bind(this));
    }

    addEventListener('resize', throttle(this.setDimensions.bind(this), 100));

    this.renderArrows();
    this.attachKeyboardHandler();
  }

  setDimensions() {
    const breakPointInfo = this.options.responsive
        .filter(br => (this.viewportWidth === br.breakpoint))[0] || { slides: 1 };

    this.slidesToShow = breakPointInfo.slides;

    const paneWidth = this.viewportWidth / breakPointInfo.slides; // 15px padding
    const paneCount = this.$panes.length;


    for (let i = 0; i < this.$panes.length; i++) {
      this.$panes[i].style.width = `${paneWidth}px`;
    }

    // set width of the carousel track
    this.$container.style.width = `${(paneWidth * paneCount)}px`;
    this.numberOfSlides = Math.ceil(this.$panes.length / this.slidesToShow);
    this.currentSlide = Math.ceil(this.currentSlide / this.numberOfSlides);
    this.transitionTo(this.currentSlide);
  }

  renderArrows() {
    const prevArrow = document.createElement('button');
    const nextArrow = document.createElement('button');
    prevArrow.className = nextArrow.className = 'carousel__arrow';

    prevArrow.className += ' carousel__arrow--left';
    nextArrow.className += ' carousel__arrow--right';

    prevArrow.innerHTML = `<i class="glyphicon glyphicon-chevron-left"></i>`;
    nextArrow.innerHTML = `<i class="glyphicon glyphicon-chevron-right"></i>`;

    prevArrow.addEventListener('click', this.prevSlide.bind(this));
    nextArrow.addEventListener('click', this.nextSlide.bind(this));

    this.$element.appendChild(prevArrow);
    this.$element.appendChild(nextArrow);
  }

  transitionTo(slideNumber) {
    this.$container.style.transform = `translate3d(-${this.viewportWidth * slideNumber}px, 0, 0)`;
  }

  nextSlide() {
    if (this.currentSlide < this.numberOfSlides - 1) {
      this.currentSlide += 1;
      this.transitionTo(this.currentSlide);
    }
  }

  prevSlide() {
    if (this.currentSlide > 0) {
      this.currentSlide -= 1;
      this.transitionTo(this.currentSlide);
    }
  }

  attachKeyboardHandler() {
    addEventListener('keydown', event => {
      const { key } = event;

      switch (key) {
        case KEYS.RIGHT:
          this.nextSlide();
          break;
        case KEYS.LEFT:
          this.prevSlide();
          break;
      }
    });
  }

  get viewportWidth() {
    return this.$element.offsetWidth;
  }

}

export default Carousel;
