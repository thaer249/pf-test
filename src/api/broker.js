const FIND_BROKER_URL = `${BASE_URL}/find-broker/ajax/search`;

export default function findBroker(page = 1) {
  return fetch(`${FIND_BROKER_URL}?page=${page}`)
    .then((response) => {
      return response.json();
    });
}
