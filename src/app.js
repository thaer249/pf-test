import './utils/polyfills';
import './app.sass';
import findBroker from './api/broker';
import BrokerList from './components/BrokerList';
import Carousel from './lib/carousel';

const appElement = document.getElementById('app');
let carousel = null;

findBroker()
  .then((response) => {
    const { data } = response;
    const brokerList = new BrokerList(data);
    appElement.innerHTML = `
      <div class="js-carousel carousel">
        ${brokerList.render()}
      </div>
    `;

    carousel = new Carousel('.js-carousel');
  });

