import './BrokerList.sass'

import BrokerItem from './BrokerItem';

class BrokerList {
  constructor(data) {
    Object.assign(this, {
      brokers: data.map( broker => {
        return new BrokerItem(broker);
      })
    });
  }

  render() {
    return `
      <ul class="broker-list list-unstyled">
        ${this.brokers.map( broker => `
          <li>
            <a href="http://propertyfinder.ae${broker.links.self}" target="_blank">${broker.render()}</a>
          </li>` ).join('')}
      </ul>
    `;
  }
}

export default BrokerList;
