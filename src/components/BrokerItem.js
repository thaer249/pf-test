import './BrokerItem.sass'

class BrokerItem {

  constructor(data) {
    Object.assign(this, data);
  }

  render() {
    return `
      <div class="broker-item thumbnail">
        <img src="${this.links.logo}" alt="${this.name} Logo" />
        <div class="caption">
          <h4 class="broker-item__name text-center">${this.name}</h4>
          <p class="broker-item__agent-count">${this.agentCount} agents</p>
          <p class="broker-item__head-office__title">head office</p>
          <p class="broker-item__head-office__value">${this.location}</p>
          <div class="broker-item__properies-info">
            <div class="broker-item__properies-info__item">
              ${this.residentialForRentCount} <br> for rent
            </div>
            <div class="broker-item__properies-info__item">
              ${this.residentialForSaleCount} <br> for sale
            </div>
            <div class="broker-item__properies-info__item">
              ${this.commercialTotalCount} <br> commercial
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

export default BrokerItem;
