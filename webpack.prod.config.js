const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'bundle.js',
    path: __dirname + '/build',
    publicPath: '/'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel' },
      { test: /\.sass$/, loader: ExtractTextPlugin.extract('style', 'css!resolve-url!sass') },
      { test: /\.woff$/,   loader: 'url?limit=10000&minetype=application/font-woff' },
      { test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/, loader: 'file' },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Brokers test | properyfinder.ae',
      template: './src/index.html'
    }),
    new webpack.DefinePlugin({
      BASE_URL: JSON.stringify('http://192.168.1.107:3030')
    }),
    new ExtractTextPlugin('app.css')
  ],
  sassLoader: {
    includePaths: [path.resolve(__dirname, './node_modules/bootstrap-sass/assets/stylesheets')]
  }
};
