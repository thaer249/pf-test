const http = require('http');
const httpProxy = require('http-proxy');

const PORT = 3030;
const proxy = httpProxy.createProxyServer({});

const server = http.createServer((req, res) => {

  // Set CORS headers
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
  res.setHeader('Access-Control-Allow-Headers', '*');

  if ( req.method === 'OPTIONS' ) {
    res.writeHead(200);
    res.end();
    return;
  }

  proxy.web(req, res, {
    target: 'https://www.propertyfinder.ae/en',
    changeOrigin: true,
  });
});

server.listen(PORT);
console.log(`PROXY running at http://localhost:${PORT}`);
