const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'bundle.js',
    path: __dirname + '/build',
    publicPath: '/'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel' },
      { test: /\.sass$/, loader: 'style!css!resolve-url!sass' },
      { test: /\.woff$/,   loader: 'url?limit=10000&minetype=application/font-woff' },
      { test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/, loader: 'file' },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Brokers test | properyfinder.ae',
      template: './src/index.html'
    }),
    new webpack.DefinePlugin({
      BASE_URL: JSON.stringify('http://localhost:3030')
    })
  ],
  sassLoader: {
    includePaths: [path.resolve(__dirname, './node_modules/bootstrap-sass/assets/stylesheets')]
  }
};
